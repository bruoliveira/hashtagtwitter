# **HashtagTwitter**

Esse sistema é uma integração com a API do Twitter que tem como objetivo coletar tweets com hashtags específicas. 
O sistema utiliza stream para fazer a coleta dos tweets em tempo real. 

Com esse sistema é possível cadastrar, editar e deletar hashtags que o usuário deseja acompanhar, e visualizar os tweets coletados
com as hashtags cadastradas. Os tweets são listados para o usuário e é possível filtrá-los por cada hashtag que foi cadastrada. 

Para o desenvolvimento do sistema foram utilizados:

1. Python (versão 3.6.6)
2. Framework VueJS (versão 3.7)
3. PostgreSQL (versão 9.6)

A aplicação está rodando em **https://hashtagtwitter.herokuapp.com/**

## Instruções

### Criar virtualenv
`pipenv --three`

### Ativar virtualenv
`pipenv shell`

### Instalar dependências
`pip3 install -r requirements.txt`

### Realizar migração do banco de dados
`python manage.py db upgrade`

### Instalar Node.js (fedora)
`sudo dnf install nodejs`

### Instalar VueJS
`npm install vue`

## Credenciais do Twitter

É necessário criar um aplicação no Twitter (https://apps.twitter.com/) para obter as chaves de autenticação
necessárias para utilizar o sistema desenvolvido. 

São 4 chaves no total: **consumer_key**, **consumer_secret**, **access_token_key** e **access_token_secret**. Essas chaves devem ser inseridas no arquivo **.env**.
Antes de rodar a aplicação, é necessário executar o comando `source .env` (dentro da virtual env) para exportar essas chaves para a aplicação.