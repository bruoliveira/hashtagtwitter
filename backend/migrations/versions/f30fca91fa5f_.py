from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

revision = 'f30fca91fa5f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('desired_hashtags',
    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('topics',
    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('tweets',
    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('username', sa.String(length=50), nullable=False),
    sa.Column('text', sa.Text(), nullable=False),
    sa.Column('date', sa.String(length=45), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('id')
    )
    op.create_table('topics_tweets',
    sa.Column('tweet_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('topic_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.ForeignKeyConstraint(['topic_id'], ['topics.id'], ),
    sa.ForeignKeyConstraint(['tweet_id'], ['tweets.id'], ),
    sa.PrimaryKeyConstraint('tweet_id', 'topic_id')
    )


def downgrade():
    op.drop_table('topics_tweets')
    op.drop_table('tweets')
    op.drop_table('topics')
    op.drop_table('desired_hashtags')
