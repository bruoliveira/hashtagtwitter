from uuid import uuid4

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from app import db

class TopicDB(db.Model):
    __tablename__ = "topics"

    id = db.Column(UUID(as_uuid=True), default=uuid4, unique=True, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    tweets = relationship("TweetDB", secondary="topics_tweets")

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name
        }


class DesiredHashtagDB(db.Model):
    __tablename__ = "desired_hashtags"

    id = db.Column(UUID(as_uuid=True), default=uuid4, unique=True, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name
        }


class TweetDB(db.Model):
    __tablename__ = "tweets"

    id = db.Column(UUID(as_uuid=True), default=uuid4, unique=True, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    text = db.Column(db.Text(), nullable=False)
    date = db.Column(db.String(45), nullable=False)
    topics = relationship("TopicDB", secondary="topics_tweets")

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "text": self.text,
            "date": self.date
        }


class TopicTweetDB(db.Model):
    __tablename__ = "topics_tweets"

    tweet_id = db.Column(UUID(as_uuid=True), db.ForeignKey("tweets.id"), primary_key=True)
    topic_id = db.Column(UUID(as_uuid=True), db.ForeignKey("topics.id"), primary_key=True)
