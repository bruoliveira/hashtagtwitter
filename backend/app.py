import itertools
import os
import re
import time
from http import HTTPStatus

import pendulum
import tweepy
from flask import Flask, abort, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import DataError, IntegrityError

import config

app = Flask(__name__)
app.config.from_object(os.environ["APP_SETTINGS"])
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
CORS(app, resources={r"/*": {"origins": "*"}})

from models import DesiredHashtagDB, TopicDB, TopicTweetDB, TweetDB

auth = tweepy.OAuthHandler(
    app.config["TWITTER_CONSUMER_KEY"], app.config["TWITTER_CONSUMER_SECRET"]
)
auth.set_access_token(
    app.config["TWITTER_ACCESS_TOKEN_KEY"], app.config["TWITTER_ACCESS_TOKEN_SECRET"]
)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

HASHTAG_REGEX = r"^[#]{1}(\w+)$"
PAGE_MAX = 20
DEFAULT_PAGE = 1


class TwitterStreamListener(tweepy.StreamListener):
    def on_status(self, tweet):
        tweet_class = Tweet()

        if hasattr(tweet, "retweeted_status"):
            if hasattr(tweet.retweeted_status, "extended_tweet"):
                retweeted_status = tweet.retweeted_status.extended_tweet
                text = retweeted_status["full_text"].encode("ascii", "ignore")
                hashtags = [
                    hashtag["text"]
                    for hashtag in retweeted_status["entities"]["hashtags"]
                ]
            else:
                retweeted_status = tweet.retweeted_status
                text = retweeted_status.text.encode("ascii", "ignore")
                hashtags = [
                    hashtag["text"] for hashtag in retweeted_status.entities["hashtags"]
                ]

        elif hasattr(tweet, "extended_tweet"):
            text = tweet.extended_tweet["full_text"].encode("ascii", "ignore")
            hashtags = [
                hashtag["text"]
                for hashtag in tweet.extended_tweet["entities"]["hashtags"]
            ]

        else:
            text = tweet.text.encode("ascii", "ignore")
            hashtags = [hashtag["text"] for hashtag in tweet.entities["hashtags"]]

        date = pendulum.instance(tweet.created_at).in_tz("America/Sao_Paulo")
        args_tweets = {
            "text": text.decode("utf-8"),
            "username": str(tweet.user.screen_name),
            "date": date.to_datetime_string(),
            "hashtags": hashtags,
        }

        if len(args_tweets["hashtags"]) > 0:
            tweet_class.create_tweet(args_tweets)

        return True

    def on_error(self, status_code):
        if status_code == 420:
            time.sleep(60)
        return True

    def on_timeout(self):
        return True


streaming_api = tweepy.streaming.Stream(
    auth=api.auth, listener=TwitterStreamListener(), timeout=60
)


class Hashtag:
    def start_stream(self):
        global streaming_api
        streaming_api = tweepy.streaming.Stream(
            auth=api.auth, listener=TwitterStreamListener(), timeout=60
        )
        hashtags = self.get_hashtag_names()
        if len(hashtags) > 0:
            streaming_api.filter(follow=None, track=hashtags, is_async=True)

    def stop_stream(self):
        global streaming_api
        streaming_api.disconnect()
        del streaming_api

    def restart_stream(self):
        self.stop_stream()
        self.start_stream()

    def _validate_hashtag(self, data):
        if data and "name" in data:
            regex = re.match(HASHTAG_REGEX, data["name"])

            if regex:
                return True

        return False

    def get_hashtag_names(self):
        hashtags_list = []
        all_hashtags = self.get_all_hashtags()

        for hashtag in all_hashtags:
            hashtags_list.append(hashtag["name"])

        return hashtags_list

    def create_hashtag(self, data):
        validate_hashtag = self._validate_hashtag(data)
        if validate_hashtag:
            new_hashtag = DesiredHashtagDB(name=data["name"])
            try:
                db.session.add(new_hashtag)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
                abort(409)

            self.restart_stream()

            return new_hashtag.serialize()
        else:
            abort(400)

    def update_hashtag(self, id, data):
        old_hashtag = self.get_one_hashtag(id)
        validate_hashtag = self._validate_hashtag(data)
        if validate_hashtag:
            try:
                old_hashtag_name = old_hashtag["name"].strip("#")
                tweets = (
                    TweetDB.query.join(TopicTweetDB)
                    .join(TopicDB)
                    .filter(TopicDB.name.ilike(old_hashtag_name))
                    .all()
                )
                for tweet in tweets:
                    db.session.delete(tweet)

                DesiredHashtagDB.query.filter(DesiredHashtagDB.id == id).update(
                    {DesiredHashtagDB.name: data["name"]}
                )
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
                abort(409)

            self.restart_stream()

            new_hashtag = self.get_one_hashtag(id)
            return new_hashtag
        else:
            abort(400)

    def delete_hashtag(self, id):
        try:
            hashtag = DesiredHashtagDB.query.filter(DesiredHashtagDB.id == id).first()
        except DataError:
            abort(404)

        hashtag_name = hashtag.name.strip("#")
        tweets = (
            TweetDB.query.join(TopicTweetDB)
            .join(TopicDB)
            .filter(TopicDB.name.ilike(hashtag_name))
            .all()
        )
        for tweet in tweets:
            db.session.delete(tweet)

        db.session.delete(hashtag)
        db.session.commit()

        self.restart_stream()

        return "deleted"

    def get_one_hashtag(self, id):
        hashtag = DesiredHashtagDB.query.filter(DesiredHashtagDB.id == id).first()
        if not hashtag:
            abort(404)

        data = hashtag.serialize()
        return data

    def get_all_hashtags(self):
        resp_hashtags = []
        hashtags = DesiredHashtagDB.query.all()
        for hashtag in hashtags:
            resp_hashtags.append(hashtag.serialize())

        return resp_hashtags


class Tweet:
    def create_tweet(self, args_tweets):
        topic = Topic()

        new_tweet = TweetDB(
            username=args_tweets["username"],
            text=args_tweets["text"],
            date=args_tweets["date"],
        )
        db.session.add(new_tweet)
        db.session.commit()

        for topic_name in args_tweets["hashtags"]:
            new_tweet.topics.append(topic.create_topic(topic_name))
        db.session.commit()

    def get_tweets(self):
        resp_tweets = []

        page = request.args.get("page")
        if page:
            page_number = int(page)
        else:
            page_number = DEFAULT_PAGE

        desired_hashtag = request.args.get("hashtag")
        if desired_hashtag:
            query = (
                TweetDB.query.join(TopicTweetDB)
                .join(TopicDB)
                .filter(TopicDB.name.ilike(desired_hashtag))
                .order_by(TweetDB.date.desc())
                .paginate(page_number, PAGE_MAX, False)
            )
            tweets = query.items
            pages = query.pages
        else:
            query = TweetDB.query.order_by(TweetDB.date.desc()).paginate(
                page_number, PAGE_MAX, False
            )
            tweets = query.items
            pages = query.pages

        for tweet in tweets:
            resp_tweets.append(tweet.serialize())

        return {"tweets": resp_tweets, "pages": pages}


class Topic:
    def create_topic(self, topic_name):
        topic = TopicDB.query.filter(TopicDB.name == topic_name).first()
        if topic:
            return topic

        db.session.begin_nested()
        try:
            topic = TopicDB(name=topic_name)
            db.session.add(topic)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            topic = TopicDB.query.filter(TopicDB.name == topic_name).first()

        return topic


@app.before_first_request
def start_twitter_stream():
    hashtag = Hashtag()
    hashtag.start_stream()


@app.route("/hashtagtwitter/hashtags/<id>", methods=["GET", "PUT", "DELETE"])
def hashtag(id):
    hashtag = Hashtag()

    if request.method == "GET":
        resp = hashtag.get_one_hashtag(id)
        return jsonify(resp)
    if request.method == "PUT":
        resp = hashtag.update_hashtag(id, request.get_json())
        return jsonify(resp)
    if request.method == "DELETE":
        return hashtag.delete_hashtag(id)


@app.route("/hashtagtwitter/hashtags", methods=["GET", "POST"])
def hashtags():
    hashtag = Hashtag()

    if request.method == "GET":
        resp = hashtag.get_all_hashtags()
        return jsonify(resp)
    if request.method == "POST":
        resp = hashtag.create_hashtag(request.get_json())
        return jsonify(resp)


@app.route("/hashtagtwitter/tweets", methods=["GET"])
def tweets():
    tweet = Tweet()

    resp = tweet.get_tweets()
    return jsonify(resp)


if __name__ == "__main__":
    app.run()
