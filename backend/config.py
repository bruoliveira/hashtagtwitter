import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    TWITTER_CONSUMER_KEY = os.environ.get("CONSUMER_KEY")
    TWITTER_CONSUMER_SECRET = os.environ.get("CONSUMER_SECRET")
    TWITTER_ACCESS_TOKEN_KEY = os.environ.get("ACCESS_TOKEN_KEY")
    TWITTER_ACCESS_TOKEN_SECRET = os.environ.get("ACCESS_TOKEN_SECRET")


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True