import Vue from 'vue';
import Router from 'vue-router';
import Hashtag from './components/Hashtag.vue';
import Tweet from './components/Tweet.vue';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/hashtags',
      name: 'hashtags',
      component: Hashtag,
    },
    {
      path: '/tweets',
      name: 'tweets',
      component: Tweet,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    },
  ],
});
